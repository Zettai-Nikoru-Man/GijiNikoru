<?php
require_once(__DIR__ . "/utils.php");

header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

$mid = $_POST['m'];
$cid = $_POST['c'];

echo json_encode(nicoru("sm" . $mid,$cid));
