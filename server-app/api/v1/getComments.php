<?php
/**
 * TBD
 */

require_once(__DIR__ . "/../../utils.php");
require_once(__DIR__ . "/../../config.php");

/**
 * コメントを取得する
 * @see https://www59.atwiki.jp/nicoapi/pages/31.html
 * @param $mid string 動画ID
 * @return array コメント
 */
function _getComments($mid)
{

    // 動画情報URLを作成する
    $url = "http://flapi.nicovideo.jp/api/getflv/$mid";

    // ID, PWを取得する
    global $niconico_id, $niconico_pw;

    // 通信コンテキストを作成する
    $context = stream_context_create(array(
        "http" => array(
            'method' => 'GET',
            'header' => implode("\r\n", array(
                'Content-Type: application/x-www-form-urlencoded',
            )),
            'content' => http_build_query(array(
                'mail_tel' => $niconico_id,
                'password' => $niconico_pw
            )),
        )
    ));

    // 動画情報を取得する
    $api_res = file_get_contents($url, false, $context);

    return array(
        'res' => $api_res,
        'header' => $http_response_header
    );
}

/**
 * メイン関数
 */
function main()
{
    $login_result = login();

    // 動画ごとのマーク済みコメント情報を取得する
    $mid = $_GET['mid'];

    $comments = _getComments($mid);

    echo '<br>1---------------------------------------------';
    hdump($comments);
    echo '<br>2---------------------------------------------';
    hdump($login_result);
}

main();

$table_content = "";

// 表示用のニコられ数テーブルを構築する
foreach($nicorare as $nc) {
    $mid = $nc['mid'];
    $cid = $nc['cid'];
    $n = $nc['n'];
    $table_content .= "
        <tr>
            <td>$mid</td>
            <td>$cid</td>
            <td>$n</td>
        </tr>
    ";
}

print "
<html>
<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<title>ニコられた</title>
	<style>
	    table {
	        border-top: 1px solid;
	        border-left: 1px solid;
	    }
	    td {
	        border-right: 1px solid;
	        border-bottom: 1px solid;
	    }
    </style>
</head>
<body>
	<table>
	    $table_content
	</table>
</body>
</html>
";
