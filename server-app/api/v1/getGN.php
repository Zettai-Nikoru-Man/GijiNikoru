<?php
/**
 * ニコる情報取得
 */

require_once(__DIR__ . "/../../utils.php");
// ヘッダ設定
header('Content-type: application/json');
header('Content-Encoding: gzip');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
// var_dump(headers_list());

// リクエストから動画IDを取得する
$mid = $_GET['a'];

// NIMJをDBから取得して返却する
echo gzencode(json_encode(getNimj($mid)), 9);
