<?php
require_once(__DIR__ . "/../../utils.php");

error_reporting(E_ALL);
ini_set('display_errors','On');

// 動画ごとのマーク済みコメント情報を取得する
$param = json_decode($_POST['param']);

$nicorare = viewNicorareta($param);

// ニコられ数テーブルDOM
$table_content = "";

// ニコられ数テーブルを構築する
foreach($nicorare as $nc) {
    $mid = $nc['mid'];
    $cid = $nc['cid'];
    $n = $nc['n'];
    $table_content .= "
        <tr>
            <td>$mid</td>
            <td>$cid</td>
            <td>$n</td>
        </tr>
    ";
}

// html出力
print "
<html>
<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<title>ニコられた</title>
	<style>
	    table {
	        border-top: 1px solid;
	        border-left: 1px solid;
	    }
	    td {
	        border-right: 1px solid;
	        border-bottom: 1px solid;
	    }
    </style>
</head>
<body>
	<table>
	    $table_content
	</table>
</body>
</html>
";
