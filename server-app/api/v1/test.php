<?php

require_once(__DIR__ . "/../../utils.php");

//error_reporting(E_ALL);
//ini_set('display_errors','On');

// ヘッダ設定
header('Content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

// 動画IDを取得する
$mid = $_GET['mid'];

// 動画IDが不正な場合は終了
if(empty($mid) && !ctype_alnum($mid)){
    return;
}

// コメントJSONを返却する
processWithLogin(function ($login_metadata) {
    global $mid;
    $video_info = getVideoInfo($login_metadata, $mid);
    $comments = getComments($login_metadata, $video_info);
    $result = "";
//    foreach($comments as $k => $v) {
//        $chat = $v->chat;
//        if ($chat != null) {
//            array_push($result, array(
//                "content" => $chat->content,
//                "no" => $chat->no,
//            ));
//        }
//    }

    // バックスラッシュをエスケープする
    $comments = str_replace('\\', '￥', $comments);

    // コメ番とコメ内容を抽出する（ヒューリスティック：コメ内容の終了判定に「"}}」を使っている）
    preg_match_all('/"no": (\d+).+?"content": "([^"]+)"\}\}/', $comments, $info, PREG_SET_ORDER);
    foreach($info as $i) {
        $result .= "\"$i[1]\":\"$i[2]\",";
    }

    // 末尾のカンマを消す
    $result = mb_substr($result , 0 , mb_strlen($result) - 1);

    // JSON化する
    $result_json = "{" . "$result" . "}";

    echo $result_json;
});
