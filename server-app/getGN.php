<?php
require_once(__DIR__ . "/utils.php");

// ヘッダ設定
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
// var_dump(headers_list());

// リクエストから動画IDを取得する
$mid = $_GET['a'];

// NIMJをDBから取得して返却する
echo json_encode(getNimj("sm" . $mid));
