<?php
/**
 * ユーティリティモジュール
 * TODO: 用途ごとに分割する
 */

// 設定ファイルを読み込む
require(__DIR__ . "/config.php");

// phpバージョンを隠蔽する
header_remove('X-Powered-By');

/**
 * NIMJを取得する
 * @param $mid string 動画ID
 * @return array NIMJ
 */
function getNimj($mid) {

	// 接続する
	try{
		$db = getDatabaseConnection();
	}catch(PDOException $e){
		return ["error"=>"db_connect_failed","description"=>$e->getMessage()];
	}

	$resArray = []; // 結果として返す変数
	$stmt = $db->prepare("SELECT comment_id AS cid, nikorare AS n FROM nikorare WHERE movie_id = ?");
	$stmt->execute([$mid]);
	$result = $stmt->fetchAll();
	foreach($result as $row) {
		$resArray[$row['cid']] = (int)$row['n'];
	}

	return $resArray;
}

/**
 * 特定のコメントのニコられ回数をカウントアップする
 * @param $mid string 動画ID
 * @param $cid string コメントID
 * @return array 更新結果を示すオブジェクト
 *          更新成功の場合
 *              result: "success"（固定値）
 *          更新失敗の場合
 *              error: エラーメッセージ
 */
function nicoru($mid,$cid){
	if($cid != (int)$cid) return ["error"=>"invalid_comment_id"];

	// DBに接続する
	try{
		$db = getDatabaseConnection();
	}catch(PDOException $e){
		return ["error"=>"db_connect_failed","description"=>$e->getMessage()];
	}

	// ニコられ情報を取得する
	$get_stmt = $db->prepare("SELECT comment_id AS cid, nikorare AS n FROM nikorare WHERE movie_id = ? and comment_id = ?");
	$get_stmt->execute([$mid,$cid]);

	// まだニコられ情報が存在しない場合、初期化する
	if (!$get_stmt->fetch()) {

		// 動画は存在するか
		if(!foundVideo($mid)){ // 存在しない
			return ["error"=>"video_not_found"]; // エラーを返す
		}
		$create_stmt = $db->prepare("INSERT INTO nikorare(movie_id,comment_id,nikorare) VALUES (?,?,?)");
		$create_stmt->execute([$mid,$cid,0]);
	}

	// ニコられを更新
	$update_stmt = $db->prepare("UPDATE nikorare SET nikorare=nikorare+1 WHERE movie_id = ? and comment_id = ?");
	$update_stmt->execute([$mid,$cid]);
	return ["result"=>"success"];
}

/**
 * DB接続を取得する
 * @return PDO PDOインスタンス
 */
function getDatabaseConnection(){
	global $db_host,$db_user,$db_pass;
	return new PDO($db_host,$db_user,$db_pass);
}

/**
 * 特定の動画IDを持つ動画が存在することを判定する
 * @param $mid string 動画ID
 * @return bool $midに対応する動画が存在する場合はtrue。そうでなければfalse。
 */
function foundVideo($mid){
	$api_res = file_get_contents("http://ext.nicovideo.jp/api/getthumbinfo/".$mid);
	$dom = new DOMDocument;
	if(!$dom->loadXML($api_res)) return false;
	$tags = $dom->getElementsByTagName("thumb");
	if(!$tags || !$tags->length) return false;
	return true;
}

/**
 * 「ニコられた」ページ表示用の情報を取得する
 * @param $param object TBD
 * @return array 「ニコられた」ページ表示用の情報
 */
function viewNicorareta($param){

	// データベースに接続する
	try {
		$db = getDatabaseConnection();
	} catch (PDOException $e) {
		return ["error" => "db_connect_failed","description" => $e->getMessage()];
	}

	$query_params = array();
	$conditions = array();

	// ニコられ数テーブルを構築する
	foreach($param as $mid => $cids) {
		foreach($cids as $cid) {
			array_push($conditions, "(movie_id = ? AND comment_id = ?)");
			array_push($query_params, $mid, $cid);
		}
	}

	// WHERE句を構成する
	$where_clause = join(" OR ", $conditions);

	$stmt = $db->prepare("SELECT movie_id AS mid, comment_id AS cid, nikorare AS n FROM nikorare WHERE $where_clause");
	$stmt->execute($query_params);
	$result = $stmt->fetchAll();

	return $result;
}

/**
 * ログインデータを取得する
 * @return array 下記の構造を持つログインデータ配列
 *          第一要素：メールアドレス
 *          第二要素：パスワード
 */
function getLoginData() {
	global $niconico_id, $niconico_pw;

	// ログインデータ
	return array(

		// メールアドレス
		"mail_tel" => $niconico_id,

		// パスワード
		"password" => $niconico_pw
	);
}

/**
 * niconicoにログインする
 * @param $login_cookie_tmp_file :ログインクッキーを格納したメタデータ
 */
function login($login_cookie_tmp_file) {
	$ch = curl_init("https://secure.nicovideo.jp/secure/login?site=niconico");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_COOKIEJAR, $login_cookie_tmp_file['uri']);  // 受信したCookieを保存
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, getLoginData());
	curl_exec($ch);
	curl_close($ch);
}

/**
 * getflv APIを使って動画情報を取得する
 * @param $login_cookie_tmp_file :ログインクッキーを格納したメタデータ
 * @param $mid :動画ID
 * @return mixed
 */
function getVideoInfo($login_cookie_tmp_file, $mid) {
	$ch = curl_init("http://flapi.nicovideo.jp/api/getflv/" . $mid);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $login_cookie_tmp_file['uri']); // $cookieFileに保存されたcookieを送信する
	$result = curl_exec($ch);
	curl_close($ch);

	//getした情報を配列に格納
	parse_str($result , $parse_result);

	return $parse_result;
}

/**
 * コメントを取得する
 * @param $login_cookie_tmp_file object ログインクッキー一時ファイル
 * @param $video_info object 動画情報
 * @return mixed コメント
 */
function getComments($login_cookie_tmp_file, $video_info) {
	$thread_id = getThreadId($video_info);
	$url = getCommentServerJsonUrl($video_info);
	$user_id = getUserId($video_info);
	$thread_key = getThreadKey($login_cookie_tmp_file, $video_info);
	$wayback_key = getWaybackKey($login_cookie_tmp_file, $video_info);
	$params = array(
		"thread" => array(
			"thread" => $thread_id,
			"version" => 20090904,
			"language" => 0,
		),
		"thread_leaves" => array(
			"thread" => $thread_id,
			"content" => "0-51:100,1000",
			"language" => 0,
			"scores" => 1,
		)
	);

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $login_cookie_tmp_file['uri']); // $cookieFileに保存されたcookieを送信する
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([$params]));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	$result = curl_exec($ch);
	curl_close($ch);

	return $result;
}

/**
 * スレッドキーを取得する
 * @param $login_cookie_tmp_file object ログインクッキー一時ファイル
 * @param $video_info object 動画情報
 * @return mixed スレッドキー
 */
function getThreadKey($login_cookie_tmp_file, $video_info) {
	$ch = curl_init("http://flapi.nicovideo.jp/api/getthreadkey?thread=" . getThreadId($video_info));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $login_cookie_tmp_file['uri']); // $cookieFileに保存されたcookieを送信する
	$result = curl_exec($ch);
	curl_close($ch);
	parse_str($result , $parse_result);
	return $parse_result;
}

/**
 * Waybackキーを取得する
 * @param $login_cookie_tmp_file object ログインクッキー一時ファイル
 * @param $video_info object 動画情報
 * @return mixed Waybackキー
 */
function getWaybackKey($login_cookie_tmp_file, $video_info) {
	$ch = curl_init("http://flapi.nicovideo.jp/api/getwaybackkey?thread=" . getThreadId($video_info));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $login_cookie_tmp_file['uri']); // $cookieFileに保存されたcookieを送信する
	$result = curl_exec($ch);
	curl_close($ch);
	parse_str($result , $parse_result);
	return $parse_result['waybackkey'];
}

/**
 * niconicoにログインした上で、指定した処理を行う
 * @param $process :実行する処理
 */
function processWithLogin($process) {
	$login_cookie_tmp_file = stream_get_meta_data($fp = tmpfile()); //cookie格納ファイルを動的に生成
	login($login_cookie_tmp_file); // ログイン
	$process($login_cookie_tmp_file); // コールバック処理を実行する
	fclose($fp); // cookie格納ファイルを閉じる
}

/**
 * 動画情報からスレッドIDを取得する
 * @param $video_info object 動画情報
 * @return mixed スレッドID
 */
function getThreadId($video_info) {
	return $video_info['thread_id'];
}

/**
 * 動画情報からサーバURLを取得する
 * @param $video_info object 動画情報
 * @return mixed サーバURL
 */
function getCommentServerUrl($video_info) {
	return $video_info['ms'];
}

/**
 * 動画情報からユーザIDを取得する
 * @param $video_info object 動画情報
 * @return mixed ユーザID
 */
function getUserId($video_info) {
	return $video_info['user_id'];
}

/**
 * 動画情報からJSON版サーバURLを取得する
 * @param $video_info object 動画情報
 * @return mixed JSON版サーバURL
 */
function getCommentServerJsonUrl($video_info) {
	return str_replace("/api/", "/api.json/", getCommentServerUrl($video_info));
}

/**
 * ハイライト付きでダンプ出力する
 * @param $var object 出力する変数
 */
function hdump($var) {
	highlight_string("\n<?php\n" . var_export($var, true));
}
