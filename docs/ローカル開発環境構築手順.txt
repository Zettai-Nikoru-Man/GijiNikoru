・本番サーバの各ソフトのバージョン
	・DBサーバ
		・ソフト：MySQL
		・バージョン：5.6

	・Webサーバ
		・ソフト：Apache 2.4.x
		・CGI：PHP 5.6

・環境構築手順
	# TODO: 上記本番サーバソフトバージョンと合わせる

	※Windows環境で実施した構築手順ですが、MacOSXでもほぼ同様のはずです。

	On host
		・GijiNikoru環境用ディレクトリを作成し、そこにGijiNikoruプロジェクトをcloneする。
			> mkdir local_GijiNikoru
			> cd local_GijiNikoru
			> git clone git@gitlab.com:Zettai-Nikoru-Man/GijiNikoru.git

		・Vagrant + VirtualBoxにより環境を構築する
			> vagrant init "bento/centos-7.2"

		・Vagrantfileにて設定する
			config.vm.network "forwarded_port", guest: 80, host: 8080
			config.vm.synced_folder "./", "/vagrant"
			config.vm.synced_folder "./GijiNikoru/server-app", "/var/www/html"

		・Vagrantを起動する
			$ vagrant up

	On vagrant
		・Apache, MySQL, PHPインストール
			・sudo
				$ sudo -i

			・Apacheインストール
				$ yum -y install httpd

			・mariadb削除
				$ yum -y remove mariadb-libs.x86_64

			・MySQLインストール
				$ yum -y install http://dev.mysql.com/get/mysql57-community-release-el7-8.noarch.rpm
				$ yum -y install mysql-community-server

			・PHPインストール
				$ yum -y install epel-release
				$ rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
				$ yum -y install --enablerepo=remi,epel,remi-php70 php php-devel php-intl php-mbstring php-pdo php-gd php-mysqlnd

		・MySQL自動起動設定
			$ systemctl enable mysql.service

		・MySQL起動
			$ systemctl start mysqld.service

		・DB構築
			・パスワード初期化と初期設定
				http://qiita.com/ksugawara61/items/336ffab798e05cae4afc

				パスワード例：Gijinikoru-0524

			・ログイン
				$ mysql -u root -p

			・データベースを作成する
				mysql> create database gijinikoru;
				mysql> use gijinikoru;
				mysql> source /vagrant/GijiNikoru/server-items/dumped.sql

		・config.phpの配置と編集
			$ cp /vagrant/GijiNikoru/server-items/config.sample.php /var/www/html/config.php
			$ vi /var/www/html/config.php
				$db_host = "mysql:host=localhost;dbname=gijinikoru";
				$db_user = "root";
				$db_pass = "<<上記で設定したパスワード>>";

		・Apache起動
			$ systemctl start httpd

		・動作確認
			http://127.0.0.1:8080/api/v1/getGN.phpにアクセスし、
			[]
			が表示されることを確認する。
