import log from "loglevel";

export default {

	// ログレベル指定
	logLevel: log.levels.SILENT,

	// 最大表示ニコられ数
	maxDisplayNikorare: 999,

	// 縦ニコるくんURL
	nikoruImage_tate: chrome.runtime.getURL("images/nikoru_tate.png"),

	POST_URL: "https://j9oi.xyz/api/v1/postGN.php",

	// デバッグフラグ
	debug: false,
};
