CREATE TABLE nicoru_comment(
	id INT NOT NULL PRIMARY KEY,
	video_id VARCHAR(20) NOT NULL,
	comment_id INT NOT NULL,
	user_id INT NOT NULL,
	created_at TIMESTAMP NOT NULL
);
